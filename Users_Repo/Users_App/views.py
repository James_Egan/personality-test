from .common.encoders import ModelEncoder
from django.http import JsonResponse, FileResponse
from django.views.decorators.http import require_http_methods
from .models import User, TestResultVO
import json
import djwto.authentication as auth
import os
from django.conf import settings


class TestResultVOEncoder(ModelEncoder):
    model = TestResultVO
    properties = [
        "test_name",
        "test_date",
        "test_result"
    ]

class UserEncoder(ModelEncoder):
    model = User
    properties = [
        "username",
        "first_name",
        "last_name", 
        "age" , 
        "friends_list",
        "friend_requests", 
        "sent_requests", 
        "profile_description", 
        "profile_photo", 
        "city", 
        "state", 
        "occupation", 
        "date_joined", 
        
        ]
    encoders = {
        "test_results": TestResultVOEncoder
    } 

@require_http_methods(["GET"])
def image_view(request, image_path, type_of_photo, username ):
    print(image_path)
    if request.method == "GET":
        file_path = os.path.join(settings.MEDIA_ROOT, type_of_photo, username, image_path)
        try:
            return FileResponse(open(file_path, 'rb'), content_type='image/jpeg')
        except:
            response = JsonResponse({"message": "something went wrong"})
            response.status_code = 400
            return response


# # Create your views here.
@require_http_methods(["GET", "POST"])
def list_all_users(request):
    if request.method == "GET":
        users = User.objects.all()
        return JsonResponse(
            {"Users": users},
            encoder=UserEncoder,
            safe=False,
        )
    else:  # POST
        try:
            content = json.loads(request.body)
            user = User.objects.create(**content)
            return JsonResponse({"user": user}, encoder=UserEncoder)
        except User.DoesNotExist:
            response = JsonResponse({"message": "something went wrong"})
            response.status_code = 400
            return response


# Create your views here.
@require_http_methods(["GET"])
def list_all_testResults(request):
    if request.method == "GET":
        tests = TestResultVO.objects.all()
        return JsonResponse(
            {"tests": tests},
            encoder=TestResultVOEncoder,
            safe=False
        ) 

# Create your views here.
@require_http_methods(["GET", "PUT", "POST", "DELETE"])
def user_detail(request, username, *args, **kwargs):
    if request.method == "GET":
        try:
            user = User.objects.get(username=username)
    
            return JsonResponse(
                {"User": user},
                encoder=UserEncoder,
            )
        except User.DoesNotExist:
            return JsonResponse({"User does not exist.": "message"})

    elif request.method == "POST":
        
        none_file_data = request.POST 
        username = none_file_data.get("username")
        photo_name = none_file_data.get("profile_photo")
        image = request.FILES.get('image')
        existing_file = none_file_data.get("existing_photo")
        user = User.objects.get(username=username)
        file_path = os.path.join(settings.MEDIA_ROOT, "profile_photo", username, existing_file)

        # print(content["profile_photo"], user.profile_photo)
        # if none_file_data["profile_photo"] != user.profile_photo:
            

        try:
            if image:
                print("HEEEEELLLLLOOOOOO")
                user.profile_photo = image
                if os.path.exists(file_path):
                    print("made it here")
                    os.remove(file_path)
     
            # Save the updated model instance
            user.save()
            #may not need next line, and instead add "user" assignment to next line
            # User.objects.filter(username=username).update(**content)
            # user = User.objects.get(username=username)
            return JsonResponse(
                user,
                encoder=UserEncoder,
                safe=False,
            )
        except User.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:

            User.objects.get(username=username).delete()
            count, _ = User.objects.filter(username=username).delete()
            return JsonResponse({"Deleted": count == 0})
        except User.DoesNotExist:
            response = JsonResponse({"message": "User does not exist, try a different username"})
        

@auth.jwt_login_required
@require_http_methods("GET")
def get_payload_token(request):
    token_data = request.payload
    userName = token_data["user"]
    if userName:
        return JsonResponse({"userName": userName})
    response = JsonResponse({"userName": None})
    return response

@require_http_methods(["GET"])
def api_user_token(request):
    if "jwt_access_token" in request.COOKIES:
        token = request.COOKIES["jwt_access_token"]
        if token:
            return JsonResponse({"token": token})
    response = JsonResponse({"token": None})
    return response

@require_http_methods(["POST"])
def create_profile_picture(request):
    if request.method == "POST":
        none_file_data = request.POST 
        username = none_file_data.get("username")
        photo_name = none_file_data.get("profile_photo")
        image = request.FILES.get('image')
        existing_file = none_file_data.get("existing_photo")
        print("THIS IS EXISTENCE", existing_file)
        old_path = os.path.join(settings.MEDIA_ROOT, "profile_photo", username)
        existing_path = os.path.join(settings.MEDIA_ROOT, "profile_photo", username)
        if not os.path.exists(existing_path):
            os.mkdir(existing_path)
        
        print(type(image))  
        # if image:
        #     # file_path = os.path.join(settings.MEDIA_ROOT,"profile_photo", username, photo_name)
        #     new_file = open(f"/app/media/profile_photo/{username}/{photo_name}", "w")
        #     new_file.write(image)
        if image:
            file_path = os.path.join(settings.MEDIA_ROOT,"profile_photo", username, photo_name)
            with open(file_path, "wb") as new_file:
                new_file.write(image.read())
            # if os.path.exists(old_path):
            #     os.remove(old_path)
            # do something with the image, for example save it to the database
            return JsonResponse({'message': 'Image uploaded successfully'})
        else:
            return JsonResponse({'message': 'No image provided'}, status=400)