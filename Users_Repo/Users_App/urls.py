from django.conf.urls.static import static
# from django.conf import settings
from django.urls import path
from .views import (
    list_all_users,
    list_all_testResults,
    user_detail,
    get_payload_token,
    api_user_token,
    image_view,
    create_profile_picture
)


urlpatterns = [
    path("", list_all_users, name="list_all_users"),
    path("user_instance/<str:username>/", user_detail, name="user_detail"),
    path("testResults/", list_all_testResults, name="list_all_testResults"),
    path("token/", get_payload_token, name="get_payload_token"),
    path("token/mine", api_user_token, name="get_my_token"),
    path('<str:type_of_photo>/<str:username>/<str:image_path>/', image_view, name='image_view'),
    path("create_profile_picture/", create_profile_picture, name="create_profile_picture")
]

# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
