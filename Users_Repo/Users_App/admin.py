from django.contrib import admin
from .models import User, TestResultVO

# Register your models here.
# Register your models here.
class UserAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)

class TestResultVOAdmin(admin.ModelAdmin):
    pass


admin.site.register(TestResultVO, TestResultVOAdmin)