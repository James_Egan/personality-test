from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import AbstractUser

class TestResultVO(models.Model):
    test_name = models.CharField(max_length=300)
    test_date = models.DateTimeField(auto_now_add=True)
    test_result = models.CharField(max_length=1000)

def upload_path(instance, filename):
    return "/".join(['profile_photo',str(instance.username),filename])

class User(AbstractUser):
    first_name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300)
    age = models.SmallIntegerField(null=True, blank=True)
    friends_list = models.ManyToManyField("self", blank=True)
    friend_requests = ArrayField(
        models.IntegerField(), blank=True, default=list
    )
    sent_requests = ArrayField(
        models.IntegerField(), blank=True, default=list
    )
    profile_description = models.TextField(null=True, blank=True)
    profile_photo = models.ImageField(null=True, blank=True, upload_to=upload_path)
    city = models.CharField(max_length=150)
    state = models.CharField(max_length=2)
    occupation = models.CharField(max_length=300, null=True, blank=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    test_results = models.ForeignKey(TestResultVO, related_name="username", on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f"Username: {self.username}, First Name: {self.first_name}, Last Name: {self.last_name}"

    def save(self, *args, **kwargs):
        if self.is_staff is not True:
            self.set_password(self.password)
            super().save(*args, **kwargs)
        else:
            super().save(*args, **kwargs)

