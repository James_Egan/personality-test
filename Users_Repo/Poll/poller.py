import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Users_Project.settings")
django.setup()

from Users_App.models import TestResultVO
TESTS_API = os.environ["TESTS_API"]

def get_test_result_vos():
    response = requests.get(f"{TESTS_API}/tests/test_result_list/")
    content = json.loads(response.content) 
    print("content", content)

    for test in content["Test_results"]:
        TestResultVO.objects.update_or_create(
            id=test["id"],
            defaults={
                "test_name": test["test_name"]["test_name"],
                "test_result": test["result"]["result_name"]
            },
        )

def poll():
    while True:
        print("polling for testVO's")
        try:
            get_test_result_vos()
            print('get test_result_vos function used')
        except Exception as e:
            print(e, file=sys.stderr)
            print("get testVO function failed")
        time.sleep(60)


if __name__ == "__main__":
    poll()
