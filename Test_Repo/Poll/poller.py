import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Test_Project.settings")
django.setup()

from Test_App.models import User_VO
USERS_API = os.environ["USERS_API"]

def get_users():
    response = requests.get(f"{USERS_API}/users/")
    content = json.loads(response.content)
   
    for user in content["Users"]:
        User_VO.objects.update_or_create(
            username=user["username"],
            defaults={
                "first_name": user["first_name"],
                "last_name": user["last_name"],
            },
        )

def poll():
    while True:
        print("polling")
        try:
            get_users()
            print('get user function used')
        except Exception as e:
            print(e, file=sys.stderr)
            print("get user function failed")
        time.sleep(60)


if __name__ == "__main__":
    poll()
