You regularly make new friends.
i/e 

You spend a lot of your free time exploring various random topics that pique your interest.
s/i

Seeing other people cry can easily make you feel like you want to cry too.
f/t

You often make a backup plan for a backup plan.
p/j

You usually stay calm, even under a lot of pressure.
h/uh

At social events, you rarely try to introduce yourself to new people and mostly talk to the ones you already know.
e/i

You prefer to completely finish one project before starting another.
p/j

You are very sentimental.
f/t

You like to use organizing tools like schedules and lists.
p/j

Even a small mistake can cause you to doubt your overall abilities and knowledge.
h/uh

You feel comfortable just walking up to someone you find interesting and striking up a conversation.
e/i

You are not too interested in discussing various interpretations and analyses of creative works.
s/i

You are more inclined to follow your head than your heart.
e/i

You usually prefer just doing what you feel like at any given moment instead of planning a particular daily routine.
p/j

You rarely worry about whether you make a good impression on people you meet.
t/f

You enjoy participating in group activities.
e/i

You like books and movies that make you come up with your own interpretation of the ending.
s/i

Your happiness comes more from helping others accomplish things than your own accomplishments.
f/t

You are interested in so many things that you find it difficult to choose what to try next.
p/j

You are prone to worrying that things will take a turn for the worse
h/uh

You avoid leadership roles in group settings.
e/i

You are definitely not an artistic type of person.
i/s

You think the world would be a better place if people relied more on rationality and less on their feelings.
f/t

prefer doing your chores before you relax
p/j

You enjoy watching people argue.
t/f

You tend to avoid drawing attention to yourself.
i/e

You lose patience with people who are not as efficient as you.
t/f

You often end up doing things at the last possible moment.
p/j

You have always been fascinated by the question of what, if anything, happens after death.
s/i

You usually prefer to be around others rather than on your own.
i/e

You become bored or lose interest when the discussion gets highly theoretical.
s/i

You find it easy to empathize with a person whose experiences are very different from yours.
f/t

You usually postpone finalizing decisions for as long as possible.
p/j

After a long and exhausting week, a lively social event is just what you need.
i/e

You enjoy going to art museums.
s/i

You often have a hard time understanding other people’s feelings.
t/f

You like to have a to-do list for each day.
p/j

You avoid making phone calls
e/i

You often spend a lot of time trying to understand views that are very different from your own.
i/s

In your social circle, you are often the one who contacts your friends and initiates activities.
p/j

If your plans are interrupted, your top priority is to get back on track as soon as possible.
p/j

You rarely contemplate the reasons for human existence or the meaning of life.
i/s

Your emotions control you more than you control them.
f/t

You take great care not to make people look bad, even when it is completely their fault.
f/t

Your personal work style is closer to spontaneous bursts of energy than organized and consistent efforts.
pj

You would love a job that requires you to work alone most of the time.
i/e

You believe that pondering abstract philosophical questions is a waste of time.
i/s

You feel more drawn to places with busy, bustling atmospheres than quiet, intimate places.
i/e

You know at first glance how someone is feeling.
i/s

You complete things methodically without skipping over any steps.
p/j

You would pass along a good opportunity if you thought someone else needed it more.
f/t

You struggle with deadlines.
p/j

