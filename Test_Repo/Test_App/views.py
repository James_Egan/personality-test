from pkgutil import extend_path
from django.shortcuts import render
from .common.encoders import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Test_Result, Test, Questions, Question_Type, Test_Result_Info, User_VO
import json
import djwto.authentication as auth

# Create your views here.
class User_VOEncoder(ModelEncoder):
    model = User_VO
    properties = [
        "username",
        "first_name",
        "last_name"
    ]


class TestEncoder(ModelEncoder):
    model = Test
    properties = [
        "test_name",
        "pk"
    ]

class TestResultInfoEncoder(ModelEncoder):
    model = Test_Result_Info
    properties = [
        "id",
        "result_name",
        "description",
        "photo",
        "famous_people"
    ]

class TestResultEncoder(ModelEncoder):
    model = Test_Result
    properties = [
        "id",
        "user",
        "result",
        "test_name"
    ]
    encoders = {
        "user": User_VOEncoder(),
        "result" :TestResultInfoEncoder(),
        "test_name": TestEncoder(),
    }


class QuestionTypeEncoder(ModelEncoder):
    model = Question_Type
    properties = [
        "question_type"
    ]


class QuestionEncoder(ModelEncoder):
    model = Questions
    properties = [
        "question",
        "test",
        "question_type",
        "pk",
        "question_direction"
    ]
    encoders = {
        "test": TestEncoder(),
        "question_type": QuestionTypeEncoder()
    }


@require_http_methods(["GET", "POST"])
def all_tests(request):
    if request.method == "GET":
            tests = Test.objects.all()
            return JsonResponse(
                {"Tests": tests},
                encoder=TestEncoder,
            )
    else:
        try:
            content = json.loads(request.body)
            Test.objects.create(**content)
            JsonResponse(
                Test,
                encoder=TestEncoder,
                safe=False
            )
        except Test.DoesNotExist:
            response = JsonResponse({"Message": "Not able to create new test"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def instance_of_test(request, pk):
    if request.method == "GET":
        try:
            content = json.loads(request.body)
            test_name = content["test_name"]
            test = Test.objects.get(test_name=test_name)
            return JsonResponse(
                {"Test": test},
                encoder=TestEncoder,
            )
        except Test.DoesNotExist:
            response =  JsonResponse({"Message": "Test type does not exist."})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            test = Test.objects.get(pk=pk)
            del test
        except Test.DoesNotExist:
            response = JsonResponse(
                {"Message": "Could not delete question"}
            )
            response.status_code = 404
            return response
    else:
        try:
            # may not work
            content = json.loads(request.body)
            test_name = content["old_name"]
            del content["old_name"]
            test = Test.objects.get(test_name=test_name).update(**content)
            return JsonResponse(
                {"New Test": test},
                encoder=TestEncoder
            )
        except Test.DoesNotExist:
            response = JsonResponse({"Message: ": "Test does not exist."})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def questions_list(request):
    if request.method == "GET":
        questions = Questions.objects.all()
        return JsonResponse(
            {"List of Questions": questions},
            encoder=QuestionEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            question = Questions.objects.create(**content)
            return JsonResponse(
                {"Question created": question}
            )
        except:
            response = JsonResponse(
                {"Message": "Could not create question"}
            )
            response.status_code = 404
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def question_detail(request, pk):
    if request.method == "GET":
        try:
            question = Questions.object.get(pk=pk)
            return JsonResponse(
                {"Question": question},
                encoder=QuestionEncoder
            )
        except Questions.DoesNotExist:
            response = JsonResponse({"Message": "Question does not exist"})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            question = Questions.objects.get(pk=pk).update(**content)
            return JsonResponse(
                {"Updated Question": question}
            )
        except Questions.DoesNotExist:
            response = JsonResponse(
                {"Message": "Could not update question"}
            )
            response.status_code = 404
            return response
    else:
        try:
            question = Questions.objects.get(pk=pk)
            del question
            return JsonResponse(
                {"Message": "Question was deleted."}
            )
        except Questions.DoesNotExist:
            response = JsonResponse(
                {"Message": "Could not delete question"}
            )
            response.status_code = 404
            return response


# is a POST in order to use the body to include test_name
@require_http_methods("POST")
def questions_for_test(request):
    try:
        content = json.loads(request.body)
        test_name = content["test_name"]
        questions = Questions.objects.filter(test__test_name=test_name)
        return JsonResponse(
            {"questions": questions},
            encoder=QuestionEncoder
        )
    except Test.DoesNotExist:
        response = JsonResponse(
            {"Message": "Could not find questions for specified test"}
        )
        response.status_code = 404
        return response

@require_http_methods("GET")
def test_result_info(request):
    try:
        test_result_info = Test_Result_Info.objects.all()
        return JsonResponse(
            {"Test_result_info": test_result_info},
            encoder=TestResultInfoEncoder
        )
    except Test.DoesNotExist:
        response = JsonResponse(
            {"Message": "Could not find test result info"}
        )
        response.status_code = 404
        return response

@require_http_methods("GET")
def test_result_info_detail(request, pk):
    try:
        test_result_info = Test_Result_Info.objects.filter(pk=pk)
        return JsonResponse(
            {"Test_result_info": test_result_info},
            encoder=TestResultInfoEncoder
        )
    except Test.DoesNotExist:
        response = JsonResponse(
            {"Message": "Could not find result info for specified PK, try a new PK"}
        )
        response.status_code = 404
        return response

  
@require_http_methods(["GET", "POST"])
def test_result_list(request):
    if request.method =="POST":
        try:
            content = json.loads(request.body)
            user = content["user"]
            username = user["username"]
            questions = content["questions"]["data"]["questions"]
            test_name = content["testName"]
            trickster_score = 0
            extroversion_score = 0
            introversion_score = 0
            intuition_score = 0
            sensing_score = 0
            feeling_score = 0
            thinking_score = 0
            judging_score = 0
            prospecting_score = 0
            
            for question in questions:
                print("question",  question)
                score = int(question["score"])
                question_type = question["question_type"]["question_type"]
                question_direction = question["question_direction"]
                if question_type == "Extroversion/Introversion":
                    if question_direction == "Extroversion":
                        if score > 3:
                            extroversion_score += score - 3
                        elif score == 3:
                            trickster_score += 1
                        else:
                            introversion_score += score
                    else:
                        if score > 3:
                            introversion_score += score - 3
                        elif score == 3:
                            trickster_score += 1
                        else:
                            extroversion_score += score
                elif question_type == "Sensing/Intuition":
                    if question_direction == "Sensing":
                        if score > 3:
                            sensing_score += score - 3
                        elif score == 3:
                            trickster_score +=1
                        else:
                            intuition_score += score
                    else:
                        if score > 3:
                            intuition_score += score - 3
                        elif score == 3:
                            trickster_score +=1
                        else:
                            sensing_score += score
                elif question_type == "Thinking/Feeling":
                    if question_direction == "Thinking":
                        if score > 3:                    
                            thinking_score += score - 3
                        elif score == 3:
                            trickster_score += 1
                        else:
                            feeling_score += score
                    else:
                        if score > 3:
                            feeling_score += score - 3
                        elif score == 3:
                            trickster_score += 1
                        else:
                            thinking_score += score
                else:
                    if question["question_direction"] == "Judging":
                        if score > 3:
                            judging_score += score - 3
                        elif score == 3:
                            trickster_score += 1
                        else:
                            prospecting_score += score
                    else:
                        if score > 3:
                            prospecting_score += score - 3
                        elif score == 3:
                            trickster_score += 1
                        else:
                            judging_score += score
            
            if trickster_score == 50:
                return JsonResponse({
                    "Test_Result_Info": "You are a trickster lol"
                })
            print(extroversion_score, introversion_score, intuition_score, sensing_score, thinking_score, feeling_score, prospecting_score, judging_score)
            result = []
            if extroversion_score > introversion_score:
                result.append("E")
            else:
                result.append("I")
            if intuition_score > sensing_score:
                result.append("N")
            else:
                result.append("S")
            if thinking_score > feeling_score:
                result.append("T")
            else:
                result.append("F")
            if judging_score > prospecting_score:
                result.append("J")
            else:
                result.append("P")

            result = "".join(result)

            create_content = {}
            create_content["result"] = Test_Result_Info.objects.get(result_name=result)
            create_content["test_name"] = Test.objects.get(test_name=test_name)
            create_content["user"] = User_VO.objects.get(username=username)
            test_result = Test_Result.objects.create(**create_content)


            return JsonResponse(
                {"Test_Result": test_result},
                encoder=TestResultEncoder
            
            )

        except:
            response = JsonResponse(
                {"Message": "Could not compute test score"}
            )
            response.status_code = 404
            return response

    else:
        try:
            test_results = Test_Result.objects.all()
            return JsonResponse(
                {"Test_results": test_results},
                encoder=TestResultEncoder
            )
        except Test_Result.DoesNotExist:
            response = JsonResponse({"Message": "Test Result does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def get_users_tests(request, username):
    try:
        print("in the view")
        return_newest_tests = []
        test_types = Test.objects.all()
        for test_type in test_types:
            instance = Test_Result.objects.filter(user__username=username).filter(test_name=test_type).order_by("-date_taken")
            if len(instance) > 0:
                return_newest_tests.append(instance[0])
            else:
                continue
        
        return JsonResponse(
            {"User Test Results": return_newest_tests},
            encoder=TestResultEncoder
        )
    
    except Test_Result.DoesNotExist:
        response = JsonResponse({"Message": "Test Results do not exist"})
        response.status_code = 404
        return response
