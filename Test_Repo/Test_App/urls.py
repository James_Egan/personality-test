from django.urls import path
from .views import (all_tests, instance_of_test, questions_for_test, questions_list, test_result_info, test_result_info_detail, test_result_list, get_users_tests)

urlpatterns = [
    path("all_tests/", all_tests, name="list_test"),
    path("specific_test/<int:pk>/", instance_of_test, name="one_test"),
    path("question_list/", questions_list, name="questions_list"),
    path("questions_for_test/", questions_for_test, name="questions_for_test"),
    path("test_result_info/", test_result_info, name="test_result_info"),
    path("test_result_info/<int:pk>/", test_result_info_detail, name= "test_result_info_detail"),
    path("test_result_list/", test_result_list, name= "test_result_list"),
    path("users_test/<str:username>/", get_users_tests, name="users_tests"),
] 