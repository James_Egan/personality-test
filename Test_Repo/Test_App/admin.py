from django.contrib import admin
from .models import User_VO, Questions

# Register your models here.
class UserVOAdmin(admin.ModelAdmin):
    pass

class QuestionsAdmin(admin.ModelAdmin):
    pass

admin.site.register(Questions, QuestionsAdmin)
admin.site.register(User_VO, UserVOAdmin)