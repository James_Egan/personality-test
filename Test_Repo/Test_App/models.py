from unittest.util import _MAX_LENGTH
from django.db import models
from django.utils.timezone import now



# Create your models here.
class Question_Type(models.Model):
    question_type = models.CharField(max_length=300)


class User_VO(models.Model):
    username = models.CharField(max_length=300)
    first_name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300)

    def __str__(self):
        return (f"Username: {self.username}, First Name: {self.first_name}, Last Name: {self.last_name}")


class Test(models.Model):
    test_name = models.CharField(max_length=300)


class Questions(models.Model):
    question = models.CharField(max_length=300)
    question_type = models.ForeignKey(
        Question_Type,
        related_name="question",
        on_delete=models.CASCADE
    )
    question_direction = models.CharField(max_length=300, null=True, default=None)
    test = models.ForeignKey(
        Test,
        null=True,
        related_name="question",
        on_delete=models.CASCADE
    )


class Test_Result_Info(models.Model):
    result_name = models.CharField(max_length=300)
    description = models.TextField()
    photo = models.URLField(null=True, blank=True)
    famous_people = models.CharField(max_length=300)
    


class Test_Result(models.Model):
    user = models.ForeignKey(
        User_VO,
        related_name="test_result",
        on_delete=models.CASCADE
    )
    result = models.ForeignKey(
        Test_Result_Info,
        related_name="test_result",
        on_delete=models.CASCADE
    )
    test_name = models.ForeignKey(
        Test,
        related_name="test_result",
        on_delete=models.CASCADE
    )
    date_taken = models.DateTimeField(auto_now_add=True)
