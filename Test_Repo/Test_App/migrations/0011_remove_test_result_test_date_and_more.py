# Generated by Django 4.0.3 on 2022-12-12 18:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Test_App', '0010_test_result_info_test_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='test_result',
            name='test_date',
        ),
        migrations.RemoveField(
            model_name='test_result_info',
            name='test_date',
        ),
    ]
