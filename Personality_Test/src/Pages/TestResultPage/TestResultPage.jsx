import { useLocation } from "react-router"
import ResultCard from "../../Components/ResultCard/ResultCard";


function TestResultPage() {

    const location = useLocation();
    const userName = location.state.Test_Result.user.username;
    const resultName = location.state.Test_Result.result.result_name;
    const description = location.state.Test_Result.result.description;
    const famousPeople = location.state.Test_Result.result.famous_people;

    return (
        <>
            <ResultCard
            userName = {userName}
            resultName = {resultName}
            description = {description}
            famousPeople = {famousPeople}
            />
        </>
    )
}

export default TestResultPage   