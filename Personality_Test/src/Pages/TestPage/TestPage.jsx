import { useParams, useNavigate } from "react-router"
import {useQuery} from '@tanstack/react-query'
import { useState, useContext } from "react"
import fetchQuestionList from "../../Fetches/FetchQuestionList"
import QuestionCard from "../../Components/QuestionCard/QuestionCard"
import { incrementButton } from "../../Utils/IncrementButton"
import fetchResultsList from "../../Fetches/FetchResultsList"
import {UserContext} from "../../Utils/UserContext.jsx"

import "../../index.css"

function TestPage () {
    const {test_name} = useParams()
    const questions = useQuery(["questions",test_name ], fetchQuestionList)
    const [result, setResult] = useState([])
    const [currentQIndex, setCurrentQIndex] = useState(0)
    const [isChecked, setIsChecked] = useState(false)
    const { userInfo } = useContext(UserContext)
    const navigate = useNavigate()

    
    function addQuestionInfo(questionInfo) {
      setResult([...result, questionInfo])
    }
    
    function handleQButton(questionInfo){
      if(isChecked === true){
        setCurrentQIndex(incrementButton(currentQIndex))
        addQuestionInfo(questionInfo)
      }
      setIsChecked(false)
    }

    function handleChecked(){
      setIsChecked(true)
      
    }

    async function handleSubmit(questionInfo){
      if(isChecked === true) {
        await addQuestionInfo(questionInfo)
        const result = await fetchResultsList(questions, userInfo, test_name)
        navigate("result", {state: result})
      }
    }

    if (questions.isLoading) {
        return (
          <div className="loader-container">
            <h2 className="loader">🌀</h2>
          </div>
        );
      }
      
      const questionList = questions?.data?.questions ?? [] 
      const questionLength = questionList.length

    return (
        <>
            <h1>{test_name} </h1>
                <div>
                    <QuestionCard 
                    completeQInfo={questionList[currentQIndex]}
                    handleQButton={handleQButton}
                    currentIndex={currentQIndex}
                    questionsLength={questionLength}
                    isChecked={isChecked}
                    handleChecked={handleChecked}
                    handleSubmit={handleSubmit}
                    />  
                </div>
        </>
    )  
}
export default TestPage
