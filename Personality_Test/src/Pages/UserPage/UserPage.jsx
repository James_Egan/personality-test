import {UserContext} from "../../Utils/UserContext.jsx"
import { useContext } from "react"

import UserAvatarCard from "../../Components/UserAvatarCard/UserAvatarCard"
import UserBio from "../../Components/UserBio/UserBio"
import UserPageTestResults from "../../Components/UserPageTestResults/UserPageTestResults.jsx"

import "./UserPage.css"

const UserPage = () => {
    const { userDetail } = useContext(UserContext)

    return (
        <div className="user-profile-card">
            <UserAvatarCard userDetail={ userDetail }/>
            <UserBio userDetail={ userDetail }/>
            <UserPageTestResults userDetails={userDetail} />
        </div>
    )
}

export default UserPage