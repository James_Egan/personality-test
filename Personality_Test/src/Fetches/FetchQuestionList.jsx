
function shuffle(response) {
  return response.questions.sort(() => Math.random() - 0.5);
}

const fetchQuestionList= async ({ queryKey }) => {

    const test_name = queryKey[1];

    const questionFetchConfig = {
        method: "POST",
        body: JSON.stringify({"test_name": test_name}),
        credentials: "include",
        headers: {
            'Content-Type': 'application/json',
        },
    }
    if (!test_name) return [];
   
    const res = await fetch("http://localhost:8000/tests/questions_for_test/", questionFetchConfig);
    
  
    if (!res.ok) {
      throw new Error(`fetch not ok`);
    }
    let response = await res.json()
    let randomQuestions = shuffle(response)
    return {"questions": randomQuestions}
  }

  
  export default fetchQuestionList;
  