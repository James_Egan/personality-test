export async function fetchTests() {
    const response = await fetch("http://localhost:8000/tests/all_tests/")
    const listOfTests = await response.json()
    return listOfTests
}