export async function fetchUserInfo () {
    const payloadTokenUrl = `${process.env.REACT_APP_USERS}/users/token/`
        const fetchConfigToken = {
          method: "get",
          credentials: "include"
        }
    const tokenResponse = await fetch(payloadTokenUrl, fetchConfigToken)
    const tokenReturned = await tokenResponse.json()
    const payload = tokenReturned["userName"]
    return payload
}