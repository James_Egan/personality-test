export async function fetchProfilePhoto(username,type_of_photo, image_path) {
    const url = `http://localhost:8080/users/${type_of_photo}/${username}/${image_path}`
    const response = await fetch(url)
    return response.url
}