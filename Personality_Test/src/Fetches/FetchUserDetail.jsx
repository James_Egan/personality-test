
export async function fetchUserDetail(username) {
    const userName = username
    const response = await fetch(`http://localhost:8080/users/user_instance/${userName}/`)
    const userDetail = await response.json()
    return userDetail
}
