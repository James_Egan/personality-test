
const fetchResultsList= async (questions, user, testName) => {

    const resultsFetchConfig = {
        method: "POST",
        body: JSON.stringify({
            "testName": testName,
            "user": user,
            "questions": questions
        }),
        credentials: "include",
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const res = await fetch("http://localhost:8000/tests/test_result_list/", resultsFetchConfig);
    
    if (!res.ok) {
      throw new Error(`fetch not ok`);
    }
    return res.json();
  }

  
  export default fetchResultsList;
  