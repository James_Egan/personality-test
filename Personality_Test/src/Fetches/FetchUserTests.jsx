

export const fetchUserTests = async (username) => {
    const userTestUrl = `http://localhost:8000/tests/users_test/${username}/`
    const response = await fetch(userTestUrl)
    const content = await response.json()
    const listOfUserTests = content["User Test Results"]
    return listOfUserTests
}