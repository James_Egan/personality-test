
export const updateUserInfo = async (username, updatedData) => {
    const response = await fetch(`http://localhost:8080/users/user_instance/${username}/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json'
      },
      body: updatedData,
    })
    if (!response.ok) {
      throw new Error(`fetch not ok`);
    }
  }
export default updateUserInfo

