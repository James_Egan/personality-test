export const GetFileName = (numBackslash,path) => {
    let count = 0
    let i = 0
    while(count < numBackslash) {
        if(path[i] === "/"){
            count++
        }
        i++
    }
    let slice = path.slice(i)
    return slice
}