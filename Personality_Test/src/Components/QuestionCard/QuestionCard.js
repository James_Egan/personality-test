import { useRef, useState } from "react";
import "./QuestionCard.css";


const QuestionCard = (props) => {
  const questionInformation = props.completeQInfo
  const handleSubmit = props.handleSubmit
  const ref1 = useRef()
  const ref2 = useRef()
  const ref3 = useRef()
  const ref4 = useRef()
  const ref5 = useRef()
  const [checked, setChecked] = useState(false)
  const [value, setValue] = useState(0)
  
  const getValue = (e) => {
    setValue(Number(e.target.value))
    props.handleChecked()
  }

  const unchecked =(e) => {
    ref1.current.checked = false
    ref2.current.checked = false
    ref3.current.checked = false
    ref4.current.checked = false
    ref5.current.checked = false
    questionInformation["score"] = value
    props.handleQButton(questionInformation)
  }

  const handleSubmitFunction = async (e) => {
    setValue(Number(e.target.value))
    questionInformation["score"] = value
    await handleSubmit(questionInformation)
    
  }

    return(
    <>
    {props.currentIndex < props.questionsLength-1?
    <div className="padding">
        <div className="question-card flex card-shadow container">
            {questionInformation.question}
            <div>
                <div className="checkboxes " onChange={getValue}>
                    <div> <input ref={ref1} type="radio" id= "1" name="val" value="1"/>1</div>
                    <div> <input ref={ref2} type="radio" id= "2" name="val" value="2"/>2</div>
                    <div> <input ref={ref3} type="radio" id= "3" name="val" value="3"/>3</div>
                    <div> <input ref={ref4} type="radio" id= "4" name="val" value="4"/>4</div>
                    <div> <input ref={ref5} type="radio" id= "5" name="val" value="5"/>5</div>
                </div>
                <div className="submit_button">
                <button onClick={e => unchecked(e)}>Next</button>
                </div> 
            
            </div>
        </div>
     </div>
    : 
    <div className="padding">
        <div className="question-card flex card-shadow container">
            {questionInformation.question}
            <div>
                <div className="checkboxes " onChange={getValue}>
                    <div> <input ref={ref1} type="radio" id= "1" name="val" value="1"/>1</div>
                    <div> <input ref={ref2} type="radio" id= "2" name="val" value="2"/>2</div>
                    <div> <input ref={ref3} type="radio" id= "3" name="val" value="3"/>3</div>
                    <div> <input ref={ref4} type="radio" id= "4" name="val" value="4"/>4</div>
                    <div> <input ref={ref5} type="radio" id= "5" name="val" value="5"/>5</div>
                </div>
                <div className="submit_button">
                <button onClick={handleSubmitFunction}>Submit</button>
                </div> 
            </div>
        </div>
     </div>
     }
     </> 
)}

export default QuestionCard