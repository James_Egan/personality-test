import UserPhoto from "../UserPhoto/UserPhoto"
import UserDetails from "../UserDetails/UserDetails"

import "./UserAvatarCard.css"

const UserAvatarCard = ({ userDetail }) => {
    
    return (
        <div className="background">
            <UserPhoto userDetail={userDetail} />
            <UserDetails userDetail={userDetail} />
        </div>
    )
    
}
export default UserAvatarCard
