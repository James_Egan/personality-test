
import "./UserBio.css"

const UserBio = ({ userDetail }) => {
    let profileDescription  = userDetail?.User?.profile_description || "N/A"
    const username = userDetail?.User?.username

    if (profileDescription === "N/A") {
        profileDescription = "Please update your profile's description with the update button."
    }

    return (
        <div className="user-bio-container">
            <div className="user-bio-title">About {username}:</div>
            <div className="user-bio-description">{profileDescription}</div>
            {/* <button onClick={handleUpdate}>h</button> */}
        </div>
    )
}

export default UserBio