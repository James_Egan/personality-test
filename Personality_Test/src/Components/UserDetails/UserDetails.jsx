import "./UserDetails.css"

const UserDetails = ( {userDetail }) => {
    let username = userDetail?.User?.username || "N/A"
    let first_name = userDetail?.User?.first_name || "N/A"
    let last_name = userDetail?.User?.last_name || "N/A"
    let age = userDetail?.User?.age || "N/A"
    let occupation = userDetail?.User?.occupation || "N/A"
    let city = userDetail?.User?.city || "N/A"
    let state = userDetail?.User?.state || "N/A"
    let date_joined = userDetail?.User?.date_joined? new Date(userDetail?.User?.date_joined).toLocaleDateString() : "N/A"

    return (
        <div className="user-details-card">
            <p className="full-name-title">Username: {username}</p>
            <div className="info-user-details-card">
                <p>Name: {first_name} {last_name}</p>
                <p>Age: {age}</p>
                <p>Occupation: {occupation}</p>
                <p>Location: {city}, {state}</p>
                <p>Date Joined: {date_joined}</p> 
            </div>
        </div>
    )
}

export default UserDetails