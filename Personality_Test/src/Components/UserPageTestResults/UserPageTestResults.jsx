import { useEffect, useState } from 'react'
import TestResultsInstance from '../TestResultsInstance/TestResultsInstance'

import { fetchUserTests } from '../../Fetches/FetchUserTests'

import "./UserPageTestResults.css"

const UserPageTestResults = ({ userDetails }) => {
    const username = userDetails?.User?.username || "N/A"
    const [displayTests, setDisplayTests] = useState([])

    useEffect(() => {
        async function getUserTests() {
            if (username !== "N/A") {

                const userTestResults = await fetchUserTests(username)
                
                setDisplayTests(userTestResults) 
            }
        }
        getUserTests()
    }, [username])    

    return (
        <div className="user-test-results-card">
            <div className="user-test-result-title">
                {username}'s Test Results
            </div>
            {
                displayTests.map(test => {
                    return (
                            <TestResultsInstance 
                            key={test.id}
                            test_name={test.test_name.test_name}
                            result={test.result.result_name}
                            description={test.result.description}
                            />
                    )
                })   
            }
        </div>
    )
}

export default UserPageTestResults