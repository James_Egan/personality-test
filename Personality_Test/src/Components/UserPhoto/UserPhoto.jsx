import "../UserPhoto/UserPhoto.css"
import {UserContext} from "../../Utils/UserContext.jsx"
import { useEffect, useState } from 'react'

import { useContext } from "react"
import Button from '../Button/Button'

import  updateUserInfo  from '../../Fetches/UpdateUserInfo'
import { fetchProfilePhoto } from '../../Fetches/FetchProfilePhoto'
import { GetFileName } from '../../Utils/GetFileName'
import blank_user_photo from '../../Assets/blank_user.png'


const UserPhoto = () => {
        
    let { userDetail, setUpdate } = useContext(UserContext)


        const [newPhoto, setNewPhoto] = useState("")
        const [imageName, setImageName] = useState("")
        
        const [displayProfilePhoto, setDisplayProfilePhoto] = useState("")

        let first_name = userDetail?.User?.first_name || "N/A"
        let last_name = userDetail?.User?.last_name || "N/A"
        let age = userDetail?.User?.age || "N/A"
        let occupation = userDetail?.User?.occupation || "N/A"
        let city = userDetail?.User?.city || "N/A"
        let state = userDetail?.User?.state || "N/A"
        let date_joined = userDetail?.User?.date_joined? new Date(userDetail?.User?.date_joined).toLocaleDateString() : "N/A"
        let profile_photo= userDetail?.User?.profile_photo
        let username = userDetail?.User?.username
        let photoType = "profile_photo"

        
    useEffect(() => {
        async function getProfilePhoto() {
            const picture = await fetchProfilePhoto(username, photoType, imageName)
            setDisplayProfilePhoto(picture)
        }
        if((username !== "" && username !== undefined) && photoType && imageName){
            getProfilePhoto()
        } else {
            setDisplayProfilePhoto(blank_user_photo)
        }
    }, [imageName, username, photoType])


    useEffect(() => {
        async function setImageNameState() {
            setImageName(GetFileName(5,profile_photo))
        }
        if((username !== "" && username !== undefined) && photoType && profile_photo){
            setImageNameState()
        }

    }, [username, profile_photo, photoType])
   

    const updateUser = async () => {
        const updateContent = new FormData()
  
        updateContent.append("profile_photo", newPhoto.name)    
        updateContent.append("username", username)
        updateContent.append("image", newPhoto)
        updateContent.append("existing_photo", imageName)
        updateContent.append("first_name", first_name)
        updateContent.append("last_name", last_name)
        updateContent.append("age", age)
        updateContent.append("occupation", occupation,)
        updateContent.append("city", city,)
        updateContent.append("state", state)
        updateContent.append("date_joined", date_joined)

        await updateUserInfo(username, updateContent)
        setUpdate(true)
    }
    

    const photoSelectedHandler = event => {
        setNewPhoto(event.target.files[0])
    }
    
    return (
        profile_photo === "" ?
        <div className='profile-img-container'>
            <img className="profile-img"
            src={displayProfilePhoto} alt={`${username}'s profile`}
            />
            <div className="accordion accordion-flush" id="accordionFlushExample">
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingOne">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            Add/Update Photo
                        </button>
                    </h2>
                    <div id="flush-collapseOne" className="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">
                            <label>
                                <input type="file" onChange={photoSelectedHandler}/>  
                            </label> 
                            <Button type="button" buttonType="upload" onClick={updateUser}> Add Photo</Button>
                        </div>
                    </div>
                </div>
            </div>      
        </div>
        :  
        <div className='profile-img-container'>
            <img className="profile-img"
            src={displayProfilePhoto} alt={`${username}'s profile`}
            />
            <div className="accordion accordion-flush" id="accordionFlushExample">
                <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingOne">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            Add/Update Photo
                        </button>
                    </h2>
                    <div id="flush-collapseOne" className="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        {/* <div className="accordion-body">
                            <label>
                                <input type="file" onChange={photoSelectedHandler}/>  
                            </label> 
                            <Button type="button" buttonType="upload" onClick={updateUser}> Update Photo</Button>
                        </div> */}
                    </div>
                </div>
            </div>      
        </div>
    ) 
}


export default UserPhoto
