import "./Button.css"

export const BUTTON_CLASSES = {
    upload: "upload-button",
    submit: "submit-button",
    next: "next-button"
}


const Button = ({children, buttonType, ...otherProps}) => {
    return (
        <button className={`button-container ${BUTTON_CLASSES[buttonType]}`} {...otherProps}>
            {children}
        </button>
    )
}

export default Button