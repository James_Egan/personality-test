

const ResultCard = ({userName,resultName,description,famousPeople}) => {
    return(
    <>
        <h1>
            Congratulations {userName}, you are a:
        </h1>
        <h2 className="result-name">
            {resultName}
        </h2>
        <h3>
            {description}
        </h3>
        <div>
            Some famous people that share this personality with you are: {famousPeople}
        </div>
    </>
    )
}

export default ResultCard