import { NavLink } from "react-router-dom";
import { useState, useEffect, useContext } from "react";


import "./Navbar.css";
import { Outlet } from "react-router-dom";
import { Fragment } from "react";
import { useToken } from "../../Utils/Authorization";
import { fetchTests } from "../../Fetches/FetchTests";

import { UserContext } from "../../Utils/UserContext";

function NavBar () {
    const [testList, setTestList] = useState([]); 
    const [token , , logout] = useToken();
    // const {username} = userInfo
    const {userInfo} = useContext(UserContext)
    const username = userInfo?.username

    useEffect (() => {
        async function getTests(){
        let listOfTests= await fetchTests()
        setTestList(listOfTests["Tests"])
    };
    getTests()
}, [token, userInfo])

async function handleClick(){
    await logout()
}
    return (
        <Fragment>
        <header>
        <nav className="navbar navbar-expand-lg navbar-light bg-light ">
        <div className="container-fluid ">
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
                </li>
                <li className="nav-item">
                    <div className="dropdown">
                        <button className="btn btn-secondary dropdown nav-bar-button" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Tests
                        </button>
                        <ul className="dropdown-menu">
                        {testList.map(test => {
                            return (   
                            <li key={test.pk}>
                            <NavLink className="nav-link" aria-current="page" to={`/test/${test.test_name}`}>{test.test_name}</NavLink>
                            </li>
                        )
                        })}
                        </ul>
                    </div>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link hover-effect" aria-current="page" to={`/user/${username}`}>My Page</NavLink>
                </li>
               {userInfo?.username? 
                <li className="nav-item" onClick={handleClick}>
                    <NavLink  className="nav-link"  aria-current="page"to="/login">Log Out</NavLink>
                </li>
                :
                <li className="nav-item">
                    <NavLink  className="nav-link"  aria-current="page" to="/login">Log In</NavLink>
                </li>
                }
            </ul>
        </div>
        </div>
        </nav>
    </header>
    <Outlet />
    </Fragment>
    )
}

export default NavBar
