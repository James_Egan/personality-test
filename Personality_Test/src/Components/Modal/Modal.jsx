import "../Modal/Modal.css"

const Modal = ({callback, description, result}) => {

  return (
    <div>
        <div className="modal-overlay">
          <div className="modal-content">
            <h1>{result}</h1>
            <p>{description}</p>
            <button className="modal-button" onClick={() => callback(false)}>Close</button>
          </div>
        </div>
    </div>
  );
};

export default Modal;
