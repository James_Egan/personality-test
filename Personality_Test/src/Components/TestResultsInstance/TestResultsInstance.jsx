import Modal from "../Modal/Modal"
import { useState } from "react";
import "../TestResultsInstance/TestResultsInstance.css"

const TestResultsInstance = ({test_name, result, description}) => {
    const [isModalOpen, setIsModalOpen] = useState(null);
    
    const handleDataFromModal = (state) => {
        setIsModalOpen(state)
    }

    return (
        <div>
            <div className="test-result-instance" onClick={() => setIsModalOpen(true)}>
                Test Type: {test_name} Result: {result}
            </div>
            {isModalOpen && 
            <Modal 
            callback = {handleDataFromModal}
            description = {description}
            result = {result}
             />}
        </div>
    )
}
export default TestResultsInstance