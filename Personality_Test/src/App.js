import { useEffect, useState } from "react";
import { Routes, Route } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { fetchUserInfo } from "./Fetches/FetchUserInfo";
import { fetchUserDetail } from "./Fetches/FetchUserDetail";
import { AuthProvider } from "./Utils/Authorization";
import { UserContext } from "./Utils/UserContext";
import updateUserInfo from "./Fetches/UpdateUserInfo";

import "./index.css";

import HomePage from "./Pages/HomePage/HomePage";
import NavBar from './Components/Navbar/NavBar';
import LogIn from "./Pages/Login/Login";
import Signup from "./Pages/Signup/Signup";
import TestPage from "./Pages/TestPage/TestPage";
import TestResultPage from "./Pages/TestResultPage/TestResultPage";
import UserPage from "./Pages/UserPage/UserPage"


import UserDetails from "./Components/UserDetails/UserDetails";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: Infinity,
      cacheTime: Infinity,
    },
  },
});

function App() {
  const [userInfo, setUserInfo] = useState(null)
  const [userDetail, setUserDetail] = useState(null)
  const username = userInfo?.username
  const [update, setUpdate] = useState(false)

useEffect(()=>{
  async function getUserInfo(){
    let infoPayload = await fetchUserInfo()
    setUserInfo(infoPayload)
    let detailPayload = await fetchUserDetail(username)
    setUserDetail(detailPayload)
    setUpdate(false)
  }  
  getUserInfo()
  }, [username, update])

  return (
    <UserContext.Provider value={{
      userInfo, setUserInfo, userDetail, setUserDetail, setUpdate
    }}>
      <AuthProvider>
          <QueryClientProvider client={queryClient}>
          <Routes>

            <Route path="/" element={<NavBar userInfo={userInfo} />} >
              <Route index element={<HomePage />} />
              <Route path="login" element={<LogIn />} />
              <Route path="signup" element={<Signup />} />
              <Route path="test/:test_name" element={<TestPage />} />
              <Route path="test/:test_name/result" element={<TestResultPage/>} />
              <Route path="user/:username" element={<UserPage />} />
              <Route path="h" element={<UserDetails />} />
            </Route>
          </Routes>
        </QueryClientProvider>
      </AuthProvider>
      </UserContext.Provider>
     
)
}

export default App;
